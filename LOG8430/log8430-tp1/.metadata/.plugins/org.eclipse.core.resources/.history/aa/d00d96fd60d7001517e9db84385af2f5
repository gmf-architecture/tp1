package View;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import Commands.*;

/***
 * Classe qui g�n�re un bouton et une bo�te de texte associ�s �
 * une commande sp�cifique
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class CommandGUIMaker extends JPanel implements Observer
{
	private static final long serialVersionUID = -2411678322602234247L;
	private JButton cmdButton;			//Bouton associ� � la commande
	private JTextField cmdTxtBox;		//Bo�te de texte associ�e � la commande
	private AbstractCommand cmd;		//Commande
	
	
	/**
	 * Constructeur par param�tre
	 * @param cmd : Commande observ�e par le CommandGUIMaker
	 */
	public CommandGUIMaker(AbstractCommand cmd)
	{
		this.initialize(cmd);
	}
	
	
	/**
	 * M�thode qui intialise le bouton et la bo�te de texte associ�s
	 * � la commande
	 * @param cmd
	 */
	private void initialize(AbstractCommand cmd)
	{
		/*
		 * Border, background and layout
		 */
		this.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.setBackground(Color.WHITE);
		this.setLayout(new GridLayout(0, 2, 10, 10));
		
		/*
		 * Commande
		 */
		this.cmd = cmd;
		if(this.cmd != null)
		{
			this.cmd.addObserver(this);
		}
		
		/*
		 * Bouton
		 */
		this.cmdButton = new JButton();
		this.cmdButton.setEnabled(false);
		this.cmdButton.setText(this.cmd.getCmdName());
		this.add(this.cmdButton);
		this.cmdButton.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(cmd != null)
				{
					cmd.executeCommand();
				}
			}
		});
		
		/*
		 * TextField
		 */
		this.cmdTxtBox = new JTextField();
		this.cmdTxtBox.setBorder(new LineBorder (new Color (0,0,0)));
		this.cmdTxtBox.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(this.cmdTxtBox);
		
	}
	
	
	/**
	 * M�thode qui met � jour l'interface graphique lorsqu'alert�
	 * par la commande
	 */
	@Override
	public void update(Observable arg0, Object arg1) 
	{
		if(this.cmd != null)
		{
			this.cmdButton.setEnabled(this.cmd.executionPossible());
			this.cmdButton.setText(this.cmd.getCmdName());
			this.cmdTxtBox.setText(this.cmd.getCmdOutcome());
		}
	}

}

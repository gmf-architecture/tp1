package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import Manager.*;


/***
 * 
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class TreeMaker extends JPanel
{
	private static final long serialVersionUID = -443901562940214036L;
	
	private CommandManager cm;				//CommandManager 
	private JTree tree;						//Vue en arbre
	private JButton btnSelect;				//Bouton pour s�lectionner la racine de l'arbre
	private JFileChooser fc;				//FileChooser
	
	
	/**
	 * Constructeur par param�tre qui se charge d'initialiser toute
	 * la vue en arbre
	 * @param cm : Gestionnaire des commandes
	 */
	public TreeMaker(CommandManager cm)
	{
		this.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.setLayout(new BorderLayout(10, 10));
		
		this.fc = new JFileChooser();
		this.cm = cm;
		this.tree = new JTree();
		
		this.generateSelectButton();
		this.buildTree();
	}
	
	
	/**
	 * M�thode qui construit la vue en arbre pour naviguer � travers
	 * le syst�me de fichiers
	 */
	private void buildTree()
	{
		this.tree.addTreeSelectionListener(new TreeSelectionListener() 
		{
			public void valueChanged(TreeSelectionEvent e) 
			{
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
	            tree.getLastSelectedPathComponent();
				
				if(node.getUserObject() instanceof FileNode)
				{
					FileNode i = (FileNode) node.getUserObject();
					File selectedFile = i.file;
					cm.setCommandPath(selectedFile.getAbsolutePath());
					System.out.println("allo");
				}
			}
		});
		tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Empty") {{}}));
		tree.setShowsRootHandles(true);
		tree.setBounds(10, 11, 209, 292);
		this.add(this.tree, BorderLayout.CENTER);
	}
	
	
	/**
	 * M�thode qui g�n�re le bouton permettant d'ouvrir la fen�tre de
	 * navigation du syst�me de fichiers
	 */
	private void generateSelectButton()
	{
		/*this.btnSelect = new JButton("Select a file or a folder");
		this.btnSelect.setPreferredSize(new Dimension(131, 50));
		this.btnSelect.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				selectTreeRoot();
			}
		});
		this.add(this.btnSelect, BorderLayout.SOUTH);*/
		
		this.btnSelect = new JButton("Select file or folder");
		this.btnSelect.setPreferredSize(new Dimension(131, 50));
		this.btnSelect.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				int returnVal = fc.showOpenDialog(TreeMaker.this);
	            if (returnVal == JFileChooser.APPROVE_OPTION) 
	            {
	                java.io.File file = fc.getSelectedFile();
	                //This is where a real application would open the file.
	                String fileName = file.getName();
	                System.out.println(fileName);

	                //JTree
	                DefaultMutableTreeNode root = new DefaultMutableTreeNode(new FileNode(file));
	                tree.setModel(new DefaultTreeModel(root));
	                CreateChildNodes ccn = new CreateChildNodes(file, root);
	                new Thread(ccn).start();   
	            } 
	            else 
	            {
	               // System.out.println("Open command cancelled by user." );
	            }
			}
		});
		this.add(this.btnSelect, BorderLayout.SOUTH);
		 
	}
	
	
	/**
	 * M�thode qui permet de s�lectionner le r�pertoire qui servira de racine
	 * � la vue en arbre
	 */
	private void selectTreeRoot()
	{
		this.fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int ret = this.fc.showOpenDialog(TreeMaker.this);
		if(ret == JFileChooser.APPROVE_OPTION)
		{
			String absPath = this.fc.getSelectedFile().getAbsolutePath();
			if(this.cm != null)
				this.cm.setCommandPath(absPath);
			
			this.tree.setModel(new DefaultTreeModel(getDirectoryTree(null, new File(absPath))));
			
			SwingWorker<DefaultTreeModel, Void> worker = new SwingWorker<DefaultTreeModel, Void>()
			{
				@Override
				public DefaultTreeModel doInBackground()
				{
					DefaultTreeModel model = new DefaultTreeModel(getDirectoryTree(null, new File(absPath)));
					return model;
				}
				
				@Override
				protected void done() 
				{
		           try 
		           {
		               tree.setModel(get());
		           } catch (Exception ignore) 
		           {
		           }
		       }
			};
			
			worker.execute();
		}
	}
	
	
	/**
	 * 
	 * @param top
	 * @param dir
	 * @return
	 */
	private DefaultMutableTreeNode getDirectoryTree(DefaultMutableTreeNode top, File dir)
	{
		DefaultMutableTreeNode currentNode;
		
		if(top != null)//null si c'est le root directory
		{
			currentNode = new DefaultMutableTreeNode(dir.getName());
			top.add(currentNode);
		}
		else
		{
			currentNode = new DefaultMutableTreeNode(dir.getAbsolutePath());
		}
		
		if(dir != null)
		{
			if(!dir.isDirectory())
				return currentNode;
			
			
			if(dir.listFiles() != null)
			{
				for(File f : dir.listFiles())
				{
					if(f.canRead())
						getDirectoryTree(currentNode, f);
				}
			}
		}
		
		return currentNode;
	}
	
	
	
	
	/**
	 * Classe interne
	 */
	public class CreateChildNodes implements Runnable 
	{

        private DefaultMutableTreeNode root;

        private File fileRoot;

        public CreateChildNodes(File fileRoot, 
                DefaultMutableTreeNode root) {
            this.fileRoot = fileRoot;
            this.root = root;
        }

        @Override
        public void run() 
        {
            createChildren(fileRoot, root);
        }

        
        private void createChildren(File fileRoot, 
                DefaultMutableTreeNode node) {
            File[] files = fileRoot.listFiles();
            if (files == null) return;

            for (File file : files) {
                DefaultMutableTreeNode childNode = 
                        new DefaultMutableTreeNode(new FileNode(file));
                node.add(childNode);
                if (file.isDirectory()) {
                    createChildren(file, childNode);
                }
            }
        }

    }

	
	/**
	 * Classe interne
	 */
    public class FileNode 
    {
        private File file;

        public FileNode(File file) 
        {
            this.file = file;
        }

        @Override
        public String toString() 
        {
            String name = file.getName();
            if (name.equals("")) 
            {
                return file.getAbsolutePath();
            } else 
            {
                return name;
            }
        }
    }

}

package View;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import Manager.*;
import Commands.*;


/***
 * Classe qui affiche tous les boutons et les bo�tes de textes associ�s aux commandes charg�es
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 *
 */
public class CommandListMaker extends JPanel
{
	private static final long serialVersionUID = -4805245584845436976L;
	private CommandManager cm;		//Gestionnaire de commandes
	
	
	/**
	 * Constructeur par param�tre qui initialise le gestionnaire de commandes
	 * @param cm : gestionnaire de commandes
	 */
	public CommandListMaker(CommandManager cm)
	{
		this.setBackground(Color.WHITE);
		this.setBounds(215, 11, 435, 490);
		this.setBorder(new EmptyBorder(15, 25, 490, 50));
		this.cm = cm;
	}
	
	
	/**
	 * M�thode qui ajoute une commande au gestionnaire de commandes et un
	 * affiche le bouton et la bo�te de texte associ�s � cette commande
	 * @param cmd : commande ajout�e
	 */
	public void addCommand(AbstractCommand cmd)
	{
		if(cmd != null)
		{
			cm.addCommand(cmd);
			CommandGUIMaker cgm = new CommandGUIMaker(cmd);
			this.add(cgm);
		}
	}
	
}

package View;

import java.awt.EventQueue;

import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTree;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JMenuBar;
import javax.swing.JCheckBox;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import Manager.*;
import Commands.*;
import Loader.*;

/**
 * Classe qui g�n�re la totalit� de l'interface graphique
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class AppWindow extends JPanel
{
	private static final long serialVersionUID = -2380666563180996462L;
	
	private JFrame frame;
	private JSplitPane splitpane;
	
	private CommandManager cm;
	private DynamicLoader dl;
	private TreeMaker tm;
	private CommandFunctionsMaker cfm;
	private CommandListMaker clm;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					AppWindow window = new AppWindow();
					window.frame.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the application.
	 */
	public AppWindow() 
	{
		initialize();
	}

	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{	
		/*
		 * Frame
		 */
		this.frame = new JFrame();
		this.frame.setBounds(600, 200, 650, 500);
		this.frame.setResizable(false);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		/*
		 * JSplitPane
		 */
		this.splitpane = new JSplitPane();
		//this.splitpane.setDividerLocation(215);
		this.splitpane.setEnabled(true);
		this.frame.getContentPane().add(splitpane, BorderLayout.CENTER);
		
		
		/*
		 * CommandManager, CommandListMaker et CommandFunctionsMaker
		 */
		this.cm = new CommandManager();
		this.clm = new CommandListMaker(this.cm);
		this.cfm = new CommandFunctionsMaker(this.cm);
		
		
		/*
		 * Chargement dynamique des classes
		 */
		this.dl = new DynamicLoader();
		this.dl.loadCommands();
		ArrayList<AbstractCommand> loadedCommands = this.dl.getCommandsToLoad();
		for(int i = 0; i < loadedCommands.size(); i++)
		{
			this.clm.addCommand(loadedCommands.get(i));
		}
		
		
		/*
		 * TreeMaker
		 */
		this.tm = new TreeMaker(this.cm);
		this.splitpane.setLeftComponent(this.tm);
		
	
		/*
		 * Affichage du CommandListMaker et du CommandFunctionsMaker
		 */
		JPanel jp = new JPanel();
		jp.setBounds(215, 11, 435, 500);
		jp.setLayout(new BorderLayout(10, 10));
		jp.add(this.cfm, BorderLayout.SOUTH);
		jp.add(this.clm, BorderLayout.NORTH);
		this.splitpane.setRightComponent(jp);
	}
	
}
	

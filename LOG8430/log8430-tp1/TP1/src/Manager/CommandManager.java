package Manager;

import java.util.*;
import Commands.*;

/**
 * Classe qui g�re l'ex�cution des commandes
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class CommandManager
{
	private boolean autoRun;						//�tat de s�lection de l'option autoRun
	private ArrayList<AbstractCommand> cmdList;		//Liste de commandes
	
	
	/**
	 * Constructeur par d�faut
	 */
	public CommandManager()
	{
		this.autoRun = false;
		cmdList = new ArrayList<AbstractCommand>();
	}
	
	
	/**
	 * M�thode qui change la s�lection de l'option autoRun
	 * @param autoRun : nouvel �tat de s�lection de autoRun
	 */
	public void setAutoRun(boolean autoRun)
	{
		this.autoRun = autoRun;
	}
	
	
	/**
	 * M�thode qui retourne l'�tat de s�lection de l'option autoRun
	 * @return
	 */
	public boolean getAutoRun()
	{
		return this.autoRun;
	}
	
	
	/**
	 * M�thode qui ajoute une commande dans la liste de commandes
	 * @param cmd : commande � ajouter
	 */
	public void addCommand(AbstractCommand cmd)
	{
		if(cmd != null)
			cmdList.add(cmd);
	}
	
	
	/**
	 * M�thode qui modifie le chemin absolu des commandes dans la liste de commandes
	 * puis ex�cute toutes les commandes si l'option autoRun est s�lectionn�e
	 * @param absPath : nouveau chemin absolu
	 */
	public void setCommandPath(String absPath)
	{
		for(int i=0; i < cmdList.size(); i++)
		{
			this.cmdList.get(i).setAbsPath(absPath);
		}
		if(this.autoRun)
			this.executeAllCommands();
	}
	
			
	/**
	 * M�thode qui ex�cute une commande donn�e et en retourne le r�sultat
	 * @param cmd : commande � ex�cuter
	 * @return
	 */
	public String executeCommand(AbstractCommand cmd)
	{
		if(cmd != null)
			return cmd.executeCommand();
		else return null;
	}
		
	
	/**
	 * M�thode qui ex�cute toutes les commandes dans la liste de commandes
	 */
	public void executeAllCommands()
	{
		for(AbstractCommand cmd : cmdList)
			cmd.executeCommand();
	}
	
}

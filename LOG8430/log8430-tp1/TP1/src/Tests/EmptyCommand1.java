package Tests;

import Commands.AbstractCommand;

/**
 * Premi�re commande vide qui ne s'ex�cute que sur des fichiers
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 *
 */
public class EmptyCommand1 extends AbstractCommand 
{

	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu de l'�l�ment s�lectionn� dans la vue en arbre
	 */
	public EmptyCommand1(String absPath)
	{
		super(absPath);
		this.setCmdName("empty1");
	}
	
	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * (dans ce cas, si le chemin absolu est celui d'un fichier)
	 */
	public boolean executionPossible() 
	{
		if(this.absPath.isEmpty() || this.absPath == null || this.getElementType() != 1)
			return false;
		return true;
	}

	/**
	 * M�thode qui ex�cute la commande si possible et en retourne le r�sultat
	 */
	public String executeCommand() 
	{
		if(executionPossible())
			return "Command 1 : this is a file";
		return null;
	}

}

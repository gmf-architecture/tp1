package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Commands.*;
import Manager.*;

/**
 * Classe de tests unitaires pour les commandes vides
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class EmptyCommandTests 
{
	String absPath;
	AbstractCommand c1;
	AbstractCommand c2;
	AbstractCommand c3;
	CommandManager cm;
	
	/**
	 * Initialisation du chemin absolu
	 */
	@Before
	public void setUp()
	{
		absPath = System.getProperty("user.dir");
	}
	
	/**
	 * Cas de test : ex�cuter EmptyCommand1 sur un r�pertoire
	 */
	@Test
	public void test1() 
	{
		cm = new CommandManager();
		c1 = new EmptyCommand1(absPath);
		assertEquals(cm.executeCommand(c1), null);
	}
	
	/**
	 * Cas de test : ex�cuter EmptyCommand1 sur un fichier
	 */
	@Test
	public void test2()
	{
		cm = new CommandManager();
		c1 = new EmptyCommand1(absPath + "\\.classpath");
		assertEquals(cm.executeCommand(c1), "Command 1 : this is a file");
	}
	
	/**
	 * Cas de test : ex�cuter EmptyCommand2 sur un r�pertoire
	 */
	@Test
	public void test3() 
	{
		cm = new CommandManager();
		c2 = new EmptyCommand2(absPath);
		assertEquals(cm.executeCommand(c2), "Command 2 : this is a folder");
	}
	
	/**
	 * Cas de test : ex�cuter EmptyCommand2 sur un fichier
	 */
	@Test
	public void test4() 
	{
		cm = new CommandManager();
		c2 = new EmptyCommand2(absPath + "\\.classpath");
		assertEquals(cm.executeCommand(c2), null);
	}
	
	/**
	 * Cas de test : ex�cuter EmptyCommand3 sur un r�pertoire
	 */
	@Test
	public void test5() 
	{
		cm = new CommandManager();
		c3 = new EmptyCommand3(absPath);
		assertEquals(cm.executeCommand(c3), "Command 3 : this is a file or a folder");
	}
	
	/**
	 * Cas de test : ex�cuter EmptyCommand3 sur un fichier
	 */
	@Test
	public void test6() 
	{
		cm = new CommandManager();
		c3 = new EmptyCommand3(absPath + "\\.classpath");
		assertEquals(cm.executeCommand(c3), "Command 3 : this is a file or a folder");
	}

}

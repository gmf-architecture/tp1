package Tests;

import Commands.AbstractCommand;

/**
 * Deuxi�me commande vide qui ne s'ex�cute que sur des r�pertoires
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 *
 */
public class EmptyCommand2 extends AbstractCommand 
{
	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu de l'�l�ment s�lectionn� dans la vue en arbre
	 */
	public EmptyCommand2(String absPath)
	{
		super(absPath);
		this.setCmdName("empty2");
	}

	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * (dans ce cas, si le chemin absolu est celui d'un r�pertoire)
	 */
	public boolean executionPossible() 
	{
		if(this.absPath.isEmpty() || this.absPath == null || this.getElementType() != 2)
			return false;
		return true;
	}

	/**
	 * M�thode qui ex�cute la commande si possible et en retourne le r�sultat
	 */
	public String executeCommand() 
	{
		if(executionPossible())
			return "Command 2 : this is a folder";
		return null;
	}

}

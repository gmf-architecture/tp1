package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Loader.*;

/***
 * Classe de tests unitaires pour le chargeur dynamique de classes
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class DynamicLoaderTests 
{

	/**
	 * Cas de test : charger les classes actuellement pr�sente dans le r�pertoire du 
	 * package "Commandes" (PrintFileNameCommand, PrintFolderNameCommand et PrintPathCommand)
	 */
	@Test
	public void test1() 
	{
		DynamicLoader dl = new DynamicLoader();
		dl.loadCommands();
		//Au moment du test, il y a trois classes concr�tes de commandes
		//dans le r�pertoire du package "Commandes"
		assertEquals(dl.getCommandsToLoad().size(), 3);
	}

}

package Tests;

import Commands.AbstractCommand;

/**
 * Troisi�me commande vide qui s'ex�cute sur des fichiers et des r�pertoires
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 *
 */
public class EmptyCommand3 extends AbstractCommand 
{

	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu de l'�l�ment s�lectionn� dans la vue en arbre
	 */
	public EmptyCommand3(String absPath)
	{
		super(absPath);
		this.setCmdName("empty3");
	}
	
	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * (dans ce cas, si le chemin absolu est celui d'un fichier ou d'un r�pertoire)
	 */
	public boolean executionPossible() 
	{
		if(this.absPath.isEmpty() || this.absPath == null || this.getElementType() == 0)
			return false;
		return true;
	}

	/**
	 * M�thode qui ex�cute la commande si possible et en retourne le r�sultat
	 */
	public String executeCommand() 
	{
		if(executionPossible())
			return "Command 3 : this is a file or a folder";
		return null;
	}

}

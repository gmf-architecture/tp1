package Commands;

/**
 * Commande qui imprime le nom d'un r�pertoire
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class PrintFolderNameCommand extends AbstractCommand
{
	private String folderName;	//Nom du r�pertoire
	
	
	/**
	 * Constructeur par d�faut
	 */
	public PrintFolderNameCommand()
	{
		super();
		this.setCmdName("Print Folder Name");
	}
	
	
	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu
	 */
	public PrintFolderNameCommand(String absPath)
	{
		super(absPath, "Print Folder Name");
	}
	
	
	/**
	 * Constructeur par param�tres qui initialise le chemin absolu
	 * et le nom de la commande
	 * @param absPath : chemin absolu
	 * @param cmdName : nom de la commande
	 */
	public PrintFolderNameCommand(String absPath, String cmdName)
	{
		super(absPath, cmdName);
	}
	
	
	/**
	 * M�thode qui modifie le nom du r�pertoire
	 * @param fileName : nouveau nom du fichier
	 */
	public void setFolderName(String fileName)
	{
		this.folderName = fileName;
	}
	
	
	/**
	 * M�thode qui retourne le nom du r�pertoire
	 * @return	fileName
	 */
	public String getFolderName()
	{
		return this.folderName;
	}
	
	
	/**
	 * M�thode qui d�termine le nom du r�pertoire � partir de son chemin absolu
	 */
	private void setFolderNameFromAbsPath()
	{
		int i = this.absPath.lastIndexOf("\\");
		if(i != -1)
			this.setFolderName(this.absPath.substring(i+1, this.absPath.length()));
	}
	
	
	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * (dans ce cas si le chemin absolu m�ne � un r�pertoire)
	 * @return true s'il s'agit d'un r�pertoire, false sinon
	 */
	public boolean executionPossible() 
	{
		if(this.absPath.isEmpty() || this.absPath == null || this.getElementType() != 2)
			return false;
		return true;
	}

	
	/**
	 * M�thode qui ex�cute la commande si possible puis en retourne le r�sultat
	 * @return	r�sultat de l'ex�cution
	 */
	public String executeCommand() 
	{
		if(!this.executionPossible())
			return null;
		
		this.setFolderNameFromAbsPath();
		this.setCmdOutcome("Folder name is " + this.folderName);
				
		return this.cmdOutcome;
	}

}

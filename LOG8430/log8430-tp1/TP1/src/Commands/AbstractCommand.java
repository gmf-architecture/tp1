package Commands;

import java.io.File;
import java.util.Observable;

/**
 * Classe abstraite des commandes 
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 *
 */
public abstract class AbstractCommand extends Observable
{	
	protected String absPath;		//Chemin absolu de l'�l�ment s�lectionn� dans la vue en arbre
	protected String cmdOutcome;	//Valeur retourn�e par l'ex�cution de la commande
	protected String cmdName;		//Nom de la commande
	
	
	/**
	 * Constructeur par d�faut
	 */
	public AbstractCommand()
	{
	}
	
	
	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu
	 */
	public AbstractCommand(String absPath)
	{
		this.absPath = absPath;
	}
	
	
	/**
	 * Constructeur par param�tres qui initialise le chemin absolu
	 * et le nom de la commande
	 * @param absPath : chemin absolu
	 * @param cmdName : nom de la commande
	 */
	public AbstractCommand(String absPath, String cmdName)
	{
		this.absPath = absPath;
		this.cmdName = cmdName;
	}
	
	
	/**
	 * M�thode qui modifie le chemin absolu
	 * @param absPath : nouveau chemin absolu
	 */
	public void setAbsPath(String absPath)
	{
		this.absPath = absPath;
		this.setCmdOutcome("");
	}
	
	
	/**
	 * M�thode qui retourne le chemin absolu
	 * @return	absPath
	 */
	public String getAbsPath()
	{
		return this.absPath;
	}
	
	
	/**
	 * M�thode qui modifie le r�sultat de l'ex�cution de la commande
	 * @param cmdReturn
	 */
	public void setCmdOutcome(String cmdOutcome)
	{
		this.cmdOutcome = cmdOutcome;
		
		this.setChanged();
		this.notifyObservers();
	}
	
	
	/**
	 * M�thode qui retourne le r�sultat de l'ex�cution de la commande
	 * @return
	 */
	public String getCmdOutcome()
	{
		return this.cmdOutcome;
	}
	
	
	/**
	 * M�thode qui modifie le nom de la commande
	 * @param cmdName : nouveau nom de la commande
	 */
	public void setCmdName(String cmdName)
	{
		this.cmdName = cmdName;
	}
	
	
	/**
	 * M�thode qui retourne le nom de la commande
	 * @return	this.cmdName
	 */
	public String getCmdName()
	{
		return this.cmdName;
	}
	
	
	/**
	 * M�thode qui d�termine le type de l'�l�ment s�lectionn� � partir du chemin absolu
	 * @return 1 si fichier, 2 si r�pertoire, 0 si autre
	 */
	protected int getElementType()
	{
		File f = new File(this.absPath);
		if(f.exists())
		{
			if(f.isFile())
				return 1;
			else if(f.isDirectory())
				return 2;
		}
		return 0;
	}
	
	
	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * @return
	 */
	public abstract boolean executionPossible();
	
	
	/**
	 * M�thode qui ex�cute la commande si possible
	 * @return
	 */
	public abstract String executeCommand();

}

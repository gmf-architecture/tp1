package Commands;

/**
 * Commande qui imprime le nom d'un fichier
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class PrintFileNameCommand extends AbstractCommand
{
	private String fileName;	//Nom du fichier
	
	
	/**
	 * Constructeur par d�faut
	 */
	public PrintFileNameCommand()
	{
		super();
		this.setCmdName("Print File Name");
	}
	
	
	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu
	 */
	public PrintFileNameCommand(String absPath)
	{
		super(absPath, "Print File Name");
	}
	
	
	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * et le nom de la commande
	 * @param absPath : chemin absolu
	 * @param cmdName : nom de la commande
	 */
	public PrintFileNameCommand(String absPath, String cmdName)
	{
		super(absPath, cmdName);
	}
		
	
	/**
	 * M�thode qui modifie le nom du fichier
	 * @param fileName : nouveau nom du fichier
	 */
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	
	
	/**
	 * M�thode qui retourne le nom du fichier
	 * @return	fileName
	 */
	public String getFileName()
	{
		return this.fileName;
	}
	
	
	/**
	 * M�thode qui d�termine le nom du fichier � partir de son chemin absolu
	 */
	private void setFileNameFromAbsPath()
	{
		int i = this.absPath.lastIndexOf("\\");
		if(i != -1)
			this.setFileName(this.absPath.substring(i+1, this.absPath.length()));
	}
	
	
	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * (dans ce cas si le chemin absolu m�ne � un fichier)
	 * @return true s'il s'agit d'un fichier, false sinon
	 */
	public boolean executionPossible() 
	{
		if(this.absPath.isEmpty() || this.absPath == null || this.getElementType() != 1)
			return false;
		return true;
	}
	
	
	/**
	 * M�thode qui ex�cute la commande si possible puis en retourne le r�sultat
	 * @return	r�sultat de l'ex�cution
	 */
	public String executeCommand() 
	{
		if(!this.executionPossible())
			return null;
		
		this.setFileNameFromAbsPath();
		this.setCmdOutcome("File name is " + this.fileName);
				
		return this.cmdOutcome;
	}
	
}

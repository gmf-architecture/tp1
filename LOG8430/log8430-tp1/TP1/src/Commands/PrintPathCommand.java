package Commands;

/**
 * Commande qui imprime le chemin absolu d'un fichier ou d'un r�pertoire
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class PrintPathCommand extends AbstractCommand
{
	/**
	 * Constructeur par d�faut
	 */
	public PrintPathCommand()
	{
		super();
		this.setCmdName("Print Path");
	}
	
	
	/**
	 * Constructeur par param�tre qui initialise le chemin absolu
	 * @param absPath : chemin absolu
	 */
	public PrintPathCommand(String absPath)
	{
		super(absPath, "Print Path");
	}

	
	/**
	 * Constructeur par param�tres qui initialise le chemin absolu
	 * et le nom de la commande
	 * @param absPath : chemin absolu
	 * @param cmdName : nom de la commande
	 */
	public PrintPathCommand(String absPath, String cmdName)
	{
		super(absPath, cmdName);
	}
	
	
	/**
	 * M�thode qui v�rifie si l'ex�cution de la commande est possible
	 * (dans ce cas si le chemin absolu m�ne � un fichier ou un r�pertoire)
	 * @return true s'il s'agit d'un fichier ou d'un r�pertoire, false sinon
	 */
	public boolean executionPossible() 
	{
		if(this.absPath.isEmpty() || this.absPath == null || this.getElementType() == 0)
			return false;
		return true;
	}
	
	
	/**
	 * M�thode qui ex�cute la commande si possible puis en retourne le r�sultat
	 * @return	r�sultat de l'ex�cution
	 */
	public String executeCommand() 
	{
		if(!this.executionPossible())
			return null;
		
		this.setCmdOutcome(this.absPath);
		
		return this.cmdOutcome;
	}

}

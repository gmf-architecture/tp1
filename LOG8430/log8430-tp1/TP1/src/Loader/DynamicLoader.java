package Loader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import Commands.*;

/***
 * Classe pour charger dynamiquement les commandes
 * @author Gabriel Amyot, Francis Rivest et Mario Saliby
 */
public class DynamicLoader extends ClassLoader
{
	private ArrayList<AbstractCommand> commands;		//Liste des commandes � charger
	private String commandPackagePath;					//Chemin absolu du package contenant les classes des commandes
	
	
	/**
	 * Constructeur par d�faut qui initialise la liste de commandes � charger
	 * ainsi que le chemin absolu du package contenant les commandes
	 */
	public DynamicLoader()
	{
		this.commands = new ArrayList<AbstractCommand>();
		this.commandPackagePath = System.getProperty("user.dir") + "\\src"+ "\\Commands";
	}
	
	
	/**
	 * M�thode pour r�cup�rer la liste des commandes � charger
	 * @return this.commands
	 */
	public ArrayList<AbstractCommand> getCommandsToLoad()
	{
		return this.commands;
	}
	
	
	/**
	 * M�thode pour modifier le chemin absolu du package contenant
	 * les commandes
	 * @param path : nouveau chemin absolu
	 */
	public void setCommandPackagePath(String path)
	{
		this.commandPackagePath = path;
	}
	
	
	/**
	 * M�thode pour r�cup�rer la liste de commandes � charger
	 * @return	this.commands
	 */
	public String getCommandPackagePath()
	{
		return this.commandPackagePath;
	}
	
	
	/**
	 * M�thode pour charger les commandes dans la liste � partir
	 * du package de commandes
	 * Source : http://www.javaworld.com/article/2077477/learn-java/java-tip-113--identify-subclasses-at-runtime.html
	 */
	public void loadCommands()
	{
		File dir = new File(this.commandPackagePath);
		if(dir.exists())
		{
			String [] files = dir.list();
			for(int i = 0; i <  files.length; i++)
			{
				if(files[i].endsWith(".java"))
				{
					String className = files[i].substring(0, files[i].length()-5);
					try 
					{
						Object o = Class.forName("Commands." + className).getDeclaredConstructor(String.class).newInstance(new String(""));
						if( o instanceof AbstractCommand)
						{
							commands.add((AbstractCommand)o);
						}
					}
					catch (ClassNotFoundException cnfex) 
					{
                        System.err.println(cnfex);
                    } 
					catch (InstantiationException iex) 
					{
                    } 
					catch (IllegalAccessException iaex) 
					{
                    } 
					catch (IllegalArgumentException e) 
					{
					
					} 
					catch (InvocationTargetException e) 
					{
						e.printStackTrace();
					} 
					catch (NoSuchMethodException e) 
					{
					
					} 
					catch (SecurityException e) 
					{
						e.printStackTrace();
					}
				}
			}
		}
	}
	
}
